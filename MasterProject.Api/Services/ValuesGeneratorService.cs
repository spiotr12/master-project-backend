﻿using System;
using System.Linq;

namespace MasterProject.Api.Services
{
    public class ValuesGeneratorService
    {
        public static string[] words =
        {
            "Marlin", "Dory", "Gill", "Bloat", "Peach", "Gurgle", "Bubbles", "Deb", "Flo", "Jacques", "Nigel", "Crush", "Coral", "Squirt", "Mr. Ray"
        };


        public static string[] Produce(string keyWord, int size, int keyWordCount)
        {
            var rnd = new Random();
            var arr = new string[size];

            for (int i = 0; i < keyWordCount; i++)
            {
                arr[i] = keyWord;
            }

            for (int i = keyWordCount; i < size; i++)
            {
                arr[i] = words[rnd.Next(0, words.Length)];
            }

            // TODO: Shuffle

            arr = arr.OrderBy(x => rnd.Next()).ToArray();

            return arr;
        }
    }
}