﻿using System;
using MasterProject.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace MasterProject.Api.Controllers
{
    [Route("api/values")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private const string KeyWord = "Nemo";

        [HttpGet]
        [Route("{size}")]
        public ActionResult GetValues([FromRoute] int size, [FromQuery] int min, [FromQuery] int max)
        {
            if (min == 0 || min > size)
            {
                min = 1;
            }

            if (max == 0 || max > size)
            {
                max = size;
            }

            if (min > max)
            {
                return BadRequest("Exception: min cannot be greater then max");
            }

            var rnd = new Random();
            var keyWordCount = rnd.Next(min, max + 1);

            var arr = ValuesGeneratorService.Produce(KeyWord, size, keyWordCount);

            var response = new
            {
                data = arr,
                keyWord = KeyWord,
                keyWordCount,
                size,
                min,
                max
            };

            return Ok(response);
        }
    }
}